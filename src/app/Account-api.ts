import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Order, OrderRow, User } from '../entities';
import { prepare } from './token';

export const accountApi = createApi({
    reducerPath: 'accountApi',
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/account', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getProfile: builder.query<User, void>({
            query: () => '/'
        }),
        updateProfile: builder.mutation<User,User>({
            query: (body) => ({
                url: '/',
                method: 'PATCH',
                body
            })
        }),
        getOrders: builder.query<Order[],void>({
            query: () => '/orders'
        }),
        getCart: builder.query<Order,void>({
            query: () => '/cart'
        }),
        addToCart: builder.mutation<Order,OrderRow>({
            query: (body) => ({
                url: '/cart',
                method: 'POST',
                body
            })
        }),
        validateCart: builder.mutation<Order,void>({
            query: () => ({
                url: '/cart/validate',
                method: 'PATCH'
            })
        }),
        deleteCart: builder.mutation<void, void>({
            query: () => ({
                url: '/cart',
                method: 'DELETE'
            })
        })
    })
});


export const {
    useGetProfileQuery,
    useUpdateProfileMutation,
    useGetOrdersQuery,
    useGetCartQuery,
    useAddToCartMutation,
    useValidateCartMutation,
    useDeleteCartMutation
} = accountApi;
