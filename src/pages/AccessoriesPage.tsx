import { Col, Row } from "react-bootstrap";
import { useGetAllAccessoriesQuery } from "../app/Article-api";
import { AccessorieCard } from "../components/AccessorieCard";

export function AccessoriesPage() {
    const {data} = useGetAllAccessoriesQuery()

    return (
        <div className="row onedog container-fluid p-0 overflow-hidden " >
            {data?.map(item => <AccessorieCard key={item.id} accessorie={item} />)}
    </div>
    
    )
}