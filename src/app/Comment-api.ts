import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Comment } from '../entities';
import { prepare } from './token';

export const commentApi = createApi({
    reducerPath: 'commentApi',
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/comment', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        postComment: builder.mutation<Comment, {id:number, comment:Comment}>({
            query: ({id,comment}) => ({
                url: '/'+id,
                method: 'POST',
                body:comment
            })
        }),
        updateComment: builder.mutation<Comment,{id:number, body:Comment}>({
            query: ({id, body}) => ({
                url: '/'+id,
                method: 'PATCH',
                body
            })
        }),
        deleteComment: builder.mutation<Comment, Comment>({
            query: (id) => ({
                url: '/'+id,
                method: 'DELETE'
            })
        })
    })
});


export const {usePostCommentMutation,useUpdateCommentMutation,useDeleteCommentMutation} = commentApi;
