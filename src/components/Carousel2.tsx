// import '../styles/Home.scss'
import './Carousel2.scss'

export function Carousel2() {
    return (
        <div className="containerCarousel">
            <div className="container text-center my-3">
                <div className="row mx-auto my-auto">
                    <div id="recipeCarousel" className="carousel slide w-100" data-ride="carousel">
                        <div className="carousel-inner w-100" role="listbox">
                            <div className="carousel-item active">
                                <div className="card">
                                    <img src="/Assets/fido.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Fido</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/autrechien.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Buddy</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/plouf.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Ruger</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                            </div>

                            <div className="carousel-item">
                                <div className="card">
                                    <img src="/Assets/chien5.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Finn</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/chien6.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Simba</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/chien7.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Prince</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                            </div>

                            <div className="carousel-item">
                                <div className="card">
                                    <img src="/Assets/chien8.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Dexter</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/chien9.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Watson</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                                <div className="card">
                                    <img src="/Assets/chien10.jpg" className="card-img-top" alt="..." />
                                    <div className="card-body">
                                        <h5 className="card-title">Peanut</h5>
                                        <p className="card-text">Ce chien est une adorable boule de poils pleine d'amour à vous offrir.</p>
                                        <a href="#" className="btn btn-primary mt-2">En savoir plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <a className="carousel-control-prev w-auto" href="#recipeCarousel" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon bg-dark border border-dark rounded-circle"
                                aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next w-auto" href="#recipeCarousel" role="button" data-slide="next">
                            <span className="carousel-control-next-icon bg-dark border border-dark rounded-circle"
                                aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}
