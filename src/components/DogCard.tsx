import { Dog, OrderRow } from "../entities"
import { Button, Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { useDeleteDogMutation } from "../app/Admin-api";
import { useAddToCartMutation } from "../app/Account-api";

interface Props {
    dog: Dog;
}

export function DogCard({ dog }: Props) {

    const user = useAppSelector(state=>state.auth.user)

    const [deleteDog, deleteQuery] =useDeleteDogMutation()
    const [addToCart, addQuery]= useAddToCartMutation();

    let history = useHistory();

    function details() {
        history.push("/dog/" + dog.id);
    }

    async function add() {
        let newRow:OrderRow={};
        newRow.dog=dog;
        newRow.quantity=1;
        if(dog.price){
        newRow.price= dog.price * newRow.quantity;
        }
        await addToCart(newRow)
    }

    async function adminDelete(event: React.FormEvent<EventTarget>){
        try {
            event.preventDefault();
            
            await deleteDog(Number(dog.id))
        } catch (error: any) {
            console.log(error.data);
        }
    }

    return (
        <Card className="col-md-4 col-sm-12 ">
            <Card.Body>
                <Card.Img variant="top" alt={dog.name} src={dog.picture} />
                <Card.Title>name: {dog.name}</Card.Title>
                <Card.Text style={{ color: 'red' }}>
                    {/* breed:{dog.breed}<br />
                    weight:{dog.weight}<br />
                    coat:{dog.coat}<br />
                    rating:{dog.rating}<br/> <br/> */}
                    prix:{dog.price} €

                </Card.Text>



            </Card.Body>
            <Card.Footer>
                <Button variant="dark" onClick={() => details()}>Voir le produit</Button>
                {user &&
                    <Button onClick={add} variant="warning">Ajouter au panier</Button>
                }
                {user?.role==='admin' &&
                    <Button onClick={adminDelete} variant="warning">Supprimer</Button>
                }
            </Card.Footer>
        </Card>
        // <Card>

        //     <Card.Img variant="top" alt={dog.name} src={dog.picture} />
        //       <Card.Body>
        //        <Card.Text>
        //            Name:{dog.name}
        //            Breed:{dog.breed}
        //        </Card.Text>
        //       </Card.Body>


        // </Card>
    )
}