import './Nav.scss'
import { Link } from 'react-router-dom';
import { logout } from "../app/auth-slice";
import { useAppDispatch, useAppSelector } from '../app/hooks';

export function Nav() {

    const user = useAppSelector(state => state.auth.user)
    let dispatch = useAppDispatch();

    function handleLogout() {
        dispatch(logout())
    }


    return (
        <div className="container-fluid px-0">
            <header>
                <div className="Navbar">
                    <div className="divtel">
                        <img className="img-tel" src="/Assets/tel.png" alt="" />
                        <p>06.12.34.56.78</p>
                    </div>


                    <Link to='/'><img className="img-logo" src="/Assets/logo.png" alt="" />
                    {/* <Route path="/dog" exact>
                            <SearchBarDog />
                    </Route> */}
                    </Link>


                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse justify-content-center" id="navbarNavAltMarkup">
                            <div className="navbar-nav align-items-center">
                                <Link className="nav-link" to='/'>
                                    <img className="img" src="/Assets/search.svg" alt="" />
                                </Link>
                                <Link className="nav-link" to='/'>
                                    <img className="img" src="/Assets/shopping-cart.svg" alt="" />
                                </Link>
                                {user ? 
                                <img className="img" onClick={handleLogout} src="/Assets/logout.png" alt="" />
                                :
                                <>
                                <Link className="nav-link" to='/login'>
                                    <img className="img" src="/Assets/login.png" alt="" />
                                </Link>
                                </>
                                }
                            </div>
                        </div>
                    </nav>


                    {/* <div className="navbar navbar-expand-lg navbar-light bg-light">
                    <a className="nav-link" >
                        <Link to='/'>
                            <img className="img" src="/Assets/search.svg" alt="" />
                        </Link>
                    </a>
                    <a className="nav-link" >
                        <Link to='/'>
                            <img className="img" src="/Assets/shopping-cart.svg" alt="" />
                        </Link>
                    </a>
                    <a className="nav-link" >
                        <Link to='/'>
                            <img className="img" src="/Assets/user.svg" alt="" />
                        </Link>
                    </a>
                </div> */}

                </div>





                <div className="navbar-nav">
                    <Link className="nav-link active" to='/dog'>Chiens</Link>
                    <Link className="nav-link" to='/accessorie'>Accessoires</Link>
                    <Link className="nav-link" to='/contact'>Alimentations</Link>

                    <Link className="nav-link" to='/admin'>Admin</Link>
                    
                   
                </div>
            </header>
        </div>
    )
}
