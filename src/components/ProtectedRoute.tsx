import { Redirect, Route, RouteProps } from "react-router-dom";
import { useAppSelector } from "../app/hooks";


export function ProtectedRoute({ children, ...rest }:RouteProps) {
    const user = useAppSelector(state=>state.auth.user)

    return (
        <Route {...rest}>
            {user !== false &&
                <>
                    {
                        user
                            ? children
                            : <Redirect to='/login' />
                    }
                </>
            }
        </Route>
    );
}
