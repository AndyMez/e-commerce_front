import { useState } from "react";
import {Image} from "react-bootstrap";
import { useParams } from "react-router-dom"
import { useGetOneAccessorieQuery } from "../app/Article-api";
import { usePostCommentMutation } from "../app/Comment-api";
import FormulaireComment from "../components/FormulaireComment";
import OneComment from "../components/OneComment";
import { Comment } from "../entities";
import '../styles/OneAccessorie.scss'

export function OneAccessoriePage() {

    const [postComment, commentQuery] = usePostCommentMutation()
    const { id } = useParams<any>();
    const { data } = useGetOneAccessorieQuery(Number(id));
    const [comment, setComment] = useState<Comment>({} as Comment);

    console.log(data);
    




    if (data) {
        return <div className="row oneaccessoire container-fluid p-0 overflow-hidden">
            <Image className="col-sm-12 col-md-6" fluid src={data.picture} alt={data.name} rounded style={{ marginTop: 10 }} />
            <div className="card col-xs-12 col-md-6">

                <div className="card-body">
                    <h5 className="card-title">{data.name}</h5>
                    <p className="card-text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Saepe aut placeat asperiores, tempore, exercitationem assumenda fuga corrupti iusto ipsum sapiente nulla quam itaque odit animi ducimus facere nobis rerum odio!
                        Similique ratione dolore quaerat adipisci minima odio facere repellendus, laboriosam suscipit qui nam ipsum deserunt. Perferendis tempora molestiae animi nemo atque? Recusandae debitis aut consectetur quis illum hic dolor nesciunt!
                        In sequi minus quae veniam! Velit, iste voluptatibus sequi molestias vitae quidem facilis vero quisquam voluptatem consequatur corrupti ex minima libero nihil ipsa tenetur repellat, unde aperiam! Iste, voluptatum ad?</p>
                </div>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item">Description : {data.description}</li>
                    <li className="list-group-item">Stock : {data.stock}</li>
                    <li className="list-group-item">Note : {data.rating}/5</li>
                </ul>

            </div>
            <div className="card commentaire col-md-11 col-sm-12">
                <h5 className="card-title">Espace commentaires</h5>
                <hr />
                {/* {data.comments?.map(comment => {
                    <OneComment key={comment.id} comment={comment}/>
                })} */}


                {data.comments?.map(comment => 
                    <OneComment key={comment.id} comment ={comment}/>
                )}
              
            </div>

            <FormulaireComment/>

        </div>


    }
    return <p>error</p>
}
