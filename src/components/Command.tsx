import '../styles/Command.scss'

export function Command() {

    return (
        <div className="container-fluid">
            <div className="row d-flex justify-content-center mt-5 mb-5">
                <div className="row col-7">
                    <div className="all-orders row flex-column">

                        <div className="one-order row d-flex align-items-center mb-2">
                            <div className="col-3">
                                <img src="/assets/fido.jpg" className="img-thumbnail" alt="..." />
                            </div>
                            <div className="col-3">
                                <p className="article-name">Article name</p>
                            </div>
                            <div className="col-3">
                                <form>
                                    <input type="number" name="quantity" id="quantity" defaultValue="1" />
                                    <label htmlFor="quantity"></label>
                                </form>
                            </div>
                            <div className="col-2">
                                <p className="price">9.99 $</p>
                            </div>
                            <div className="col-1">
                                <button>x</button>
                            </div>
                        </div>

                        <div className="one-order row d-flex align-items-center mb-2">
                            <div className="col-3">
                                <img src="/assets/fido.jpg" className="img-thumbnail" alt="..." />
                            </div>
                            <div className="col-3">
                                <p className="article-name">Article name</p>
                            </div>
                            <div className="col-3">
                                <form>
                                    <input type="number" name="quantity" id="quantity" defaultValue="1" />
                                    <label htmlFor="quantity"></label>
                                </form>
                            </div>
                            <div className="col-2">
                                <p className="price">9.99 $</p>
                            </div>
                            <div className="col-1">
                                <button>x</button>
                            </div>
                        </div>

                        <div className="one-order row d-flex align-items-center mb-2">
                            <div className="col-3">
                                <img src="/assets/fido.jpg" className="img-thumbnail" alt="..." />
                            </div>
                            <div className="col-3">
                                <p className="article-name">Article name</p>
                            </div>
                            <div className="col-3">
                                <form>
                                    <input type="number" name="quantity" id="quantity" defaultValue="1" />
                                    <label htmlFor="quantity"></label>
                                </form>
                            </div>
                            <div className="col-2">
                                <p className="price">9.99 $</p>
                            </div>
                            <div className="col-1">
                                <button>x</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="result-command row col-3">
                    <div className="command-details">
                        <h1>Résumé</h1>
                        <div>
                            <h3>Prix total</h3>
                            <span>9.99 $</span>
                        </div>
                    </div>
                    <div className="d-flex justify-content-end">
                        <button className="valid-command">Valider</button>
                    </div>
                </div>
            </div>
        </div>
    )
}




