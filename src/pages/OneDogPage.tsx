import { Button, Col, Container, Image, Row } from "react-bootstrap";
import { useParams } from "react-router-dom"
import { RouterParams } from "../App"
import { useGetOneDogQuery } from "../app/Article-api";
import '../styles/OneDog.scss'

export function OneDogPage() {

    const { id } = useParams<any>();
    const { data } = useGetOneDogQuery(Number(id));

    if (data) {
        return <div className="row onedog container-fluid p-0 overflow-hidden" >
            <Image className="col-md-6 col-sm-12"fluid src={data.picture} alt={data.name} rounded style={{ marginTop: 10 }} />
            <div className="card col-md-6 col-sm-12 ">
                
                <div className ="card-body">
                <h5 className ="card-title">{data.name}</h5>
                <p className ="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam eligendi temporibus vitae accusamus nesciunt officiis odit maiores dolor, doloremque dicta distinctio ipsa ducimus corporis minus. Voluptatem, perferendis sapiente. Illum, voluptates!
                Molestiae, labore doloremque aperiam porro totam, doloribus id nemo neque iste, voluptates quo! Iusto saepe voluptate explicabo impedit recusandae veniam dicta! Quasi esse labore voluptatum suscipit, mollitia eos corrupti culpa!
                Illum enim nesciunt, rem odit placeat quaerat odio molestiae tempore doloremque deserunt repudiandae labore laudantium alias, corporis recusandae illo! Mollitia delectus consequuntur quasi voluptates consectetur sit, dolore minima quis magnam.</p>
                </div>
                <ul className ="list-group list-group-flush">
                <li className ="list-group-item">Race : {data.breed}</li>
                <li className ="list-group-item">Poids : {data.weight}</li>
                <li className="list-group-item">Poil : {data.coat}</li>
                </ul>
            </div>
        </div>

        // <Container fluid>
        //     <Row className="justify-content-center">
        //         <Col xs={10}>
        //             <Row>
        //                 <Col sm={4}>
        //                     <Image fluid src={data.picture} alt={data.name} rounded style={{ marginTop: 10 }} />
        //                 </Col>
        //                 <Col sm={8}>
        //                     <h1>{data.name}</h1>
        //                     breed:{data.breed}<br />
        //                     weight:{data.weight}<br />
        //                     coat:{data.coat}<br />
        //                     rating:{data.rating}<br /> <br />

        //                     {data.comments?.map(comment => <p key={comment.id}>
        //                         <b>Commentaires:</b> {comment.text}<br />
        //                         <b>Date:</b> {comment.date}<br />
        //                         <b>auteur:</b> {comment.user?.name}<br />
        //                         <b>Rating:</b>{comment.rating}<br /> <img src={"https://i.stack.imgur.com/cEQfe.png"} style={{ height: 100 }} />

        //                     </p>)}<br />
        //                     {/* <Button variant="warning">Ajouter au panier</Button> */}
        //                 </Col>
        //             </Row>
        //         </Col>
        //     </Row>

        // </Container>
    }
    return <p>error</p>
}
