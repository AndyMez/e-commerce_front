import { Card, Col, Container } from 'react-bootstrap'
import './Footer.scss'
import '../styles/Home.scss'

export function Footer() {
    return (
        <div className="container-fluid px-0">
            <img src="/Assets/chien1.png" alt="" />
            <img src="/Assets/chien2.png" alt="" />
            <img src="/Assets/chien3.png" alt="" />
            <img src="/Assets/chien4.png" alt="" />
            <div className="row">
                <div className="carre1 col-12 row p-3">
                    <div className="adress col-sm-12 col-md-4 d-flex align-items-center p-2">
                        <img className="coordonnées" src="/Assets/pin.svg" alt="pin" />
                        <p>34 Rue Antoine Primat</p>
                    </div>
                    <div className="mail col-sm-12 col-md-4 d-flex align-items-center p-2">
                        <img className="coordonnées" src="/Assets/mail.svg" alt="mail" />
                        <p>Doggybag@gmail.com</p>
                    </div>
                    <div className="telephone col-sm-12 col-md-4 d-flex align-items-center p-2">
                        <img className="coordonnées" src="/Assets/phone-call.svg" alt="phone-call" />
                        <p>06.12.34.56.78</p>
                    </div>
                </div>
            </div>
            <div className="row pl-3">
                <div className="carre2 row">
                    <div className="informations col-sm-12 col-md-6 pt-3 ">
                        <h3 className="pb-3">Informations</h3>
                        <p>A propos de nous</p>
                        <p>Information de livraison</p>
                        <p>Politique de confidentialité</p>
                        <p>Nous contacter</p>
                    </div>
                    <div className="compte col-sm-12 col-md-6 pt-3 mb-3">
                        <h3 className="pb-3">Mon compte</h3>
                        <p>Mon compte</p>
                        <p>Historique des commandes</p>
                        <p>Liste des souhaits</p>
                        <p>Newsletters</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
