import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Accessorie, Category, Dog, User } from '../entities';
import { prepare } from './token';

export const adminApi = createApi({
    reducerPath: 'adminApi',
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/admin', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getOneUser: builder.query<User, number>({
            query: (id) => '/user/'+id
        }),
        postDog: builder.mutation<Dog, Dog>({
            query: (body) => ({
                url: '/article/dog',
                method: 'POST',
                body
            })
        }),
        postAccessorie: builder.mutation<Accessorie, Accessorie>({
            query: (body) => ({
                url: '/article/accessorie',
                method: 'POST',
                body
            })
        }),
        updateDog: builder.mutation<Dog,{id:number, body:Dog}>({
            query: ({id, body}) => ({
                url: '/article/dog/'+id,
                method: 'PATCH',
                body
            })
        }),
        updateAccessorie: builder.mutation<Accessorie,{id:number, body:Accessorie}>({
            query: ({id, body}) => ({
                url: '/article/accessorie/'+id,
                method: 'PATCH',
                body
            })
        }),
        deleteDog: builder.mutation<void, number>({
            query: (id) => ({
                url: '/article/dog/'+id,
                method: 'DELETE'
            })
        }),
        deleteAccessorie: builder.mutation<void, number>({
            query: (id) => ({
                url: '/article/accessorie/'+id,
                method: 'DELETE'
            })
        }),
        postCategory: builder.mutation<Category, Category>({
            query: (body) => ({
                url: '/category',
                method: 'POST',
                body
            })
        }),
        updateCategory: builder.mutation<Category,{id:number, body:Category}>({
            query: ({id, body}) => ({
                url: '/category/'+id,
                method: 'PATCH',
                body
            })
        }),
        deleteCategory: builder.mutation<Category, Category>({
            query: (id) => ({
                url: '/category/'+id,
                method: 'DELETE'
            })
        }),
    })
});


export const {
    useGetOneUserQuery,
    usePostDogMutation,
    usePostAccessorieMutation,
    useUpdateDogMutation,
    useUpdateAccessorieMutation,
    useDeleteDogMutation,
    useDeleteAccessorieMutation,
    usePostCategoryMutation,
    useUpdateCategoryMutation,
    useDeleteCategoryMutation
} = adminApi;
