import { useState } from "react";
import { Form, FloatingLabel, Button } from "react-bootstrap";
import { Link, Redirect } from "react-router-dom";
import { setCredentials } from "../app/auth-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { useUserLoginMutation } from "../app/User-api";
import { User } from "../entities";

export function LoginPage() {
    let user = useAppSelector(state => state.auth.user)

    const [postLogin, postQuery] = useUserLoginMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch()
    /**
     * Post the form to the server
     * @param event 
     */
    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
            console.log(form);

            const data = await postLogin(form).unwrap()

            console.log(data);

            if (data && !postQuery.isError) {
                dispatch(setCredentials(data))
            }
        } catch (error: any) {
            console.log(error.data);

        }

    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }


        setForm(change)
    }

    return (
        !user ?
            <div>
                <h2 className="text-center">Connectez-vous à votre compte.</h2>
                
<br />
                <Form onSubmit={handleSubmit}>
                    <>
                    <div className="row d-flex justify-content-center">
                    <div className=" col-md-6">
                    <FloatingLabel className="col-md-12" controlId="floatingInput" label="Adresse mail" >
                            <Form.Control name='email' onChange={handleChange} type="email" placeholder="name@example.com" />
                        </FloatingLabel>

                        <FloatingLabel className="col-md-12" controlId="floatingPassword" label="Mot de passe">
                            <Form.Control name='password' onChange={handleChange} type="password" placeholder="Mot de passe" />
                        </FloatingLabel>
                    </div>
                        

                        </div>
                        

                    <br />

                    </>
                    <div className="d-flex justify-content-center">
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                    </div>

                </Form>
                <br />
                <br />
                <h2 className="text-center">Vous n’avez pas encore de compte ?</h2>
                <br />
                <br />
                <div className="text-center">
                <Link to='/register'>
                <Button>
                    ClickMe
                </Button>
                </Link>
                </div>


            </div> : <Redirect to='/' />
    )
}