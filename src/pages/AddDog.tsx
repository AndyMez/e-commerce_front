import { useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { usePostDogMutation } from "../app/Admin-api";
import { useGetOneDogQuery } from "../app/Article-api";
import { Dog } from "../entities";




export function AddDog() {
    const [postDog, dogQuery] = usePostDogMutation()
    const { id } = useParams<any>();
    const { data } = useGetOneDogQuery(Number(id));
    const [dog, setDog] = useState<Dog>({} as Dog);

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...dog, [name]: value }


        setDog(change)
    }

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();

            await postDog(dog)
            

        } catch (error: any) {
            console.log(error.data);

        }

    }
    return (
        <Form onSubmit={handleSubmit}>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    Picture
                </Form.Label>
                <Col sm={10}>
                <Form.Control name="picture" type="file" onChange={handleChange} placeholder="picture" />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    Name
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="name" type="text" onChange={handleChange} placeholder="name" />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    price
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="price" type="number" onChange={handleChange} placeholder="price" />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    Breed
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="breed" type="text" onChange={handleChange} placeholder="breed" />
                </Col>
            </Form.Group>
            
            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    weight
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="weight" type="text" onChange={handleChange} placeholder="weight" />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    coat
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="coat" type="text" onChange={handleChange} placeholder="coat" />
                </Col>
            </Form.Group>

            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    price
                </Form.Label>
                <Col sm={10}>
                    <Form.Control name="stock" type="number" onChange={handleChange} placeholder="stock" />
                </Col>
            </Form.Group>


            <Form.Group as={Row} className="mb-3" >
                <Form.Label column sm={2}>
                    Category
                </Form.Label>
                <Col sm={10}>
                    <Form.Select aria-label="Default select example">
                        <option value="1">Accessorie</option>
                        <option value="2">Dog</option>
                    </Form.Select>
                </Col>
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>

    )

}