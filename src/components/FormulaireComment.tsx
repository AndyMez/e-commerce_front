import { useState } from "react";
import { useParams } from "react-router";
import { usePostCommentMutation } from "../app/Comment-api";
import { useAppSelector } from "../app/hooks";
import { Comment } from "../entities";

export default function FormulaireComment(){
    const [postComment, commentQuery] = usePostCommentMutation()
    const { id } = useParams<any>();
    const [comment, setComment] = useState<Comment>({} as Comment);
    const user = useAppSelector(state => state.auth.user)


    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...comment, [name]: value }


        setComment(change)
    }

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
            
            await postComment({id,comment})

        } catch (error: any) {
            console.log(error.data);

        }
    }

    return(
        
        
        <>
                {user &&
            <form onSubmit={handleSubmit} >
            <div className="form-group inputcomment col-md-11 col-sm-12 d-flex">
                
                <textarea name="text" onChange={handleChange} className="form-control" id="exampleFormControlTextarea1" ></textarea>
                <button> Submit </button>
            </div>
            </form>
        }
</>
    )

}