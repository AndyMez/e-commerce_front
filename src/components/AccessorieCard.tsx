import { Accessorie, OrderRow } from "../entities"
import { Button, Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { useAppSelector } from "../app/hooks";
import { useDeleteAccessorieMutation } from "../app/Admin-api";
import { useAddToCartMutation } from "../app/Account-api";

interface Props {
    accessorie: Accessorie;

}



export function AccessorieCard({ accessorie }: Props) {

    const user = useAppSelector(state=>state.auth.user)

    const [deleteAccessorie, deleteQuery] = useDeleteAccessorieMutation();
    const [addToCart, addQuery]= useAddToCartMutation();

    let history = useHistory();

    function details() {
        history.push("/accessorie/" + accessorie.id);
    }

    async function add() {
        let newRow:OrderRow={};
        newRow.accessorie=accessorie;
        newRow.quantity=1;
        if(accessorie.price){
        newRow.price= accessorie.price * newRow.quantity;
        }
        await addToCart(newRow)
    }

    async function adminDelete(event: React.FormEvent<EventTarget>){
        try {
            event.preventDefault();
            
            await deleteAccessorie(Number(accessorie.id))
        } catch (error: any) {
            console.log(error.data);
        }
    }

    return (
        <Card className="col-md-4 col-sm-12" >
            <Card.Body>
                <Card.Img variant="top" alt={accessorie.name} src={accessorie.picture} />
                <Card.Title>{accessorie.name}</Card.Title>
                <Card.Text style={{ color: 'red' }}>

                    {accessorie.price} €

                </Card.Text>

            </Card.Body>
            <Card.Footer>
                <Button variant="dark" onClick={() => details()}>Voir le produit</Button>
                {user &&
                    <Button onClick={add} variant="warning">Ajouter au panier</Button>
                }
                {user?.role==='admin' &&
                    <Button onClick={adminDelete} variant="warning">Supprimer</Button>
                }
            </Card.Footer>
        </Card>
        // <Card>

        //     <Card.Img variant="top" alt={dog.name} src={dog.picture} />
        //       <Card.Body>
        //        <Card.Text>
        //            Name:{dog.name}
        //            Breed:{dog.breed}
        //        </Card.Text>
        //       </Card.Body>


        // </Card>
    )
}