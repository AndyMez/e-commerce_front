import { useState } from "react";
import { Button, Col, FloatingLabel, Form, Row } from "react-bootstrap";
import { setCredentials } from "../app/auth-slice";
import { useAppDispatch } from "../app/hooks";
import { useUserRegisterMutation } from "../app/User-api";
import { User } from "../entities";
import '../styles/App.scss'
import '../styles/Register.scss'

export function RegisterPage() {

    const [registerUser,{isError, isSuccess}] = useUserRegisterMutation();
    const [form, setForm] = useState<User>({} as User);
    let dispatch = useAppDispatch();

    const handleSubmit = async (event: React.FormEvent<EventTarget>) => {
        try {
            event.preventDefault();
            // console.log(form);
            
            await registerUser(form).unwrap();

            
            
        } catch (error: any) {
            console.log(error.data);

        }

    }

    const handleChange = (event: React.FormEvent<EventTarget>) => {
        let target = event.target as HTMLInputElement;
        let name = target.name;
        let value = target.value
        let change = { ...form, [name]: value }

        setForm(change)
    }

    return (
        <div className="register-text">
            <h2 className="text-center">Inscrivez-vous</h2>
            <br />

            <Form onSubmit={handleSubmit}>
            <div className="row d-flex justify-content-center">
                    <div className=" col-md-6">
                            <FloatingLabel className="col-md-12" controlId="floatingName" label="Nom">
                                <Form.Control name='name' onChange={handleChange} type="nom" placeholder="Nom" />
                            </FloatingLabel>



                            <FloatingLabel className="col-md-12" controlId="floatingFirstName" label="Prénom">
                                <Form.Control name='firstName' onChange={handleChange} type="prenom" placeholder="Prénom" />
                            </FloatingLabel>


                            <FloatingLabel className="col-md-12" controlId="floatingInput" label="Adresse mail" >
                                <Form.Control name='email' onChange={handleChange} type="email" placeholder="name@example.com" />
                            </FloatingLabel>



                            <FloatingLabel className="col-md-12" controlId="floatingPassword" label="Mot de passe">
                                <Form.Control name='password' onChange={handleChange} type="password" placeholder="Mot de passe" />
                            </FloatingLabel>
                    </div>
                    </div>
                    <br />
                    <div className="d-flex justify-content-center">
                    <Button variant="primary" type="submit">
                    Submit
                </Button>
                    </div>

            </Form>

        </div>
    )
}

