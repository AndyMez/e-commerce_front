// import { Carousel } from '../components/Carousel'
import { Carousel2 } from '../components/Carousel2'
import { Footer } from '../components/Footer'
import '../styles/App.scss'
import '../styles/Home.scss'

export function HomePage() {
    return (
        <div className="container-fluid p-0 overflow-hidden">
            {/* Services */}

            <div className="services row pt-4">
                <img src="/images/poor-dog.png" alt="services-dog" className="col-md-6 col-sm-12" />

                <div className="col-md-6 col-sm-12">
                    <img src="/images/DoggyBAG.png" alt="Logo-name" />
                    <h2>Services &#38; Adoption de chiens</h2>
                    <p>Donner un toit à l'une de ces magnifiques boules de poils. Elles n'attendent que vous et votre amour ! </p>
                    <p>Retrouvez nos produits et nos services pour chien.</p>
                    <p>Petit ou grand, vous trouverez votre bonheur pour votre boule de poil favorite !</p>
                    <button className="mt-4">En savoir plus</button>
                </div>
            </div>

            {/* Carousel */}
            <Carousel2 />


            {/* Help us */}
            <div className="help-us row">
                <p>Aidez notre refuge avec nos produits et ceux de nos partenaires</p>
                <div className="how-to-help row">
                    <div className="col-md-3 col-xs-6 align-items-center">
                        <img src="/images/bone.svg" alt="bone" className="help-images test2" />
                        <span>Alimentation</span>
                    </div>
                    <div className="col-md-3 col-xs-6  align-items-center">
                        <img src="/images/box.svg" alt="box" className="help-images" />
                        <span>Jouets</span>
                    </div>
                    <div className="col-md-3 col-xs-6  align-items-center">
                        <img src="/images/dog-collar.svg" alt="collar" className="help-images" />
                        <span>Colliers</span>
                    </div>
                    <div className="col-md-3 col-xs-6  align-items-center">
                        <img src="/images/shampoo.svg" alt="shampoo" className="help-images" />
                        <span>Shampoings</span>
                    </div>
                </div>
            </div>

            {/*Footer */}
            <Footer />
        </div>
    )
}
