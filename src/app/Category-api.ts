import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Category } from '../entities';
import { prepare } from './token';

export const categoryApi = createApi({
    reducerPath: 'categoryApi',
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/category', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllCategories: builder.query<Category[], void>({
            query: () =>  '/category'
        }),
        getOneCategory: builder.query<Category, number>({
            query: (id) => '/category/'+id,
        })
    })
});


export const {
    useGetAllCategoriesQuery,
    useGetOneCategoryQuery
} = categoryApi;