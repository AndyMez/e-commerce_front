import React, { useState } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { HomePage } from './pages/HomePage';
import { DogsPage } from './pages/DogsPage';
import { InformationsPage } from './pages/InformationPage';
import { LoginPage } from './pages/LoginPage';
import { RegisterPage } from './pages/RegisterPage';
import { Nav } from './components/Nav';
import { Footer } from './components/Footer';
import { ContactPage } from './pages/ContactPage';
import { OneDogPage } from './pages/OneDogPage';
import { OneAccessoriePage } from './pages/OneAccessoriePage';
import { AccessoriesPage } from './pages/AccessoriesPage';
import { useGetThisUserQuery } from './app/User-api';
import { AddAccessorie } from './pages/AddAccessorie';
import { Button } from 'react-bootstrap';
import { AddDog } from './pages/AddDog';
import { CommandPage } from './pages/CommandPage';


export interface RouterParams {
  id: string,
  search: string

}

function App() {

  const [open, SetOpen] = useState(false)

  useGetThisUserQuery(undefined, { skip: localStorage.getItem('token') === null });
  return (

    <BrowserRouter>
      <div>
        <Nav />
        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>


          <Route path="/dog" exact>
            <DogsPage />
          </Route>
          <Route path="/dog/:id">
            <OneDogPage />
          </Route>

          <Route path="/accessorie" exact>
            <AccessoriesPage />
          </Route>
          <Route path="/accessorie/:id">
            <OneAccessoriePage />
          </Route>

         
          <Route path="/admin" exact>
            
            {open && 
             <AddAccessorie/> 
            //  <AddDog/>
            }
          <Button variant="outline-dark" onClick={()=> SetOpen(true)}>Ajouter un accessoire</Button>
          <Button variant="outline-dark">Ajouter un chien</Button>
          </Route>

          <Route path="/foods">
            {/* <FoodsPage/> */}
          </Route>

          <Route path="/informations">
            <InformationsPage />
          </Route>

          <Route path="/login">
            <LoginPage />
          </Route>

          <Route path="/register">
            <RegisterPage />
          </Route>

          <Route path="/contact">
            <ContactPage />
          </Route>

          <Route path="/command">
            <CommandPage />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
