import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';
import { Accessorie, Dog } from '../entities';
import { prepare } from './token';

export const articleApi = createApi({
    reducerPath: 'articleApi',
    tagTypes: ['DogList','AccessorieList'],
    baseQuery: fetchBaseQuery({baseUrl: process.env.REACT_APP_SERVER_URL +'/api/article', prepareHeaders: prepare}),
    endpoints: (builder) => ({
        getAllDogs: builder.query<Dog[], void>({
            query: () =>  '/dog',
            providesTags: ['DogList']
        }),
        getAllAccessories: builder.query<Accessorie[], void>({
            query: () => '/accessorie',
            providesTags: ['AccessorieList']
        }),
        getOneDog: builder.query<Dog, number>({
            query: (id) => '/dog/'+id,
        }),
        getOneAccessorie: builder.query<Accessorie, number>({
            query: (id) => '/accessorie/'+id,
        }),
        searchDogs: builder.query<Dog[], string>({
            query: (search) =>  '/dog?search='+search,
            providesTags: ['DogList']
        }),
        searchAccessories: builder.query<Accessorie[], string>({
            query: (search) =>  '/accessorie?search='+search,
            providesTags: ['AccessorieList']
        })
    })
});


export const {
    useGetAllAccessoriesQuery,
    useGetAllDogsQuery,
    useGetOneAccessorieQuery,
    useGetOneDogQuery,
    useSearchAccessoriesQuery,
    useSearchDogsQuery
} = articleApi;