export interface User{
    id?: number;
    email?: string;
    password?:string;
    name?: string;
    firstName?: string;
    adress?: string;
    creditCard?: number;
    role?: string;
    lastConnection?: number;
    avatar?: string;
    status?: number;
    comments?:Comment[];
    orders?:Order[];
}

export interface Comment {
    id?: number;
    text?: string;
    date?: number;
    status?:number;
    rating?:number;
    user?: User;
    accessorie?: Accessorie;
}

export interface Accessorie extends Article{
    description?: string;
    stock?: number;
    category?:Category[];
    comments?:Comment[];
    orderRows?:OrderRow[];
}

export interface Dog extends Article{
    breed?: string;
    weight?: number;
    coat?: string;
    category?:Category;
    orderRows?:OrderRow[];
}

export interface Article{
    id?: number;
    picture?: string;
    name?: string;
    price?: number;
    rating?: number;
    status?: number;
}

export interface Category{
    id?: number;
    name?: string;
    status?: number;
    accessories?:Accessorie[];
    dogs?:Dog[];
}

export interface Order{
    id?: number;
    price?: number;
    date?: number;
    status?:number;
    user?: User;
    orderRows?:OrderRow[]
}

export interface OrderRow{
    id?: number;
    price?: number;
    quantity?: number;
    order?:Order;
    accessorie?:Accessorie;
    dog?:Dog;
}