import { Col, Row } from "react-bootstrap";
import { useGetAllDogsQuery } from "../app/Article-api";
import { DogCard } from "../components/DogCard";

export function DogsPage() {
    const { data} = useGetAllDogsQuery();

    return (
        <div className = "container-fluid p-0 overflow-hidden">
            <div className="row">
            {data?.map(item => <DogCard key={item.id} dog={item} />)}
            </div>

    </div>
    
    )
}