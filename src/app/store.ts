import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { accountApi } from './Account-api';
import { adminApi } from './Admin-api';
import { articleApi } from './Article-api';
import authSlice from './auth-slice';
import { categoryApi } from './Category-api';
import { commentApi } from './Comment-api';
import { userApi } from './User-api';

export const store = configureStore({
  reducer: {
    // [dogApi.reducerPath]:dogApi.reducer,
    // [accessorieApi.reducerPath]:accessorieApi.reducer,

    [accountApi.reducerPath]:accountApi.reducer,
    [adminApi.reducerPath]:adminApi.reducer,
    [articleApi.reducerPath]:articleApi.reducer,
    [categoryApi.reducerPath]:categoryApi.reducer,
    [commentApi.reducerPath]:commentApi.reducer,
    [userApi.reducerPath]:userApi.reducer,
    auth:authSlice
  },
  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(
    accountApi.middleware, 
    adminApi.middleware, 
    articleApi.middleware,
    categoryApi.middleware,
    commentApi.middleware,
    userApi.middleware
  ),

});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
