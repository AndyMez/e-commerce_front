import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../entities';
import { AuthState } from './auth-slice';
import { prepare } from './token';



export const userApi = createApi({
    reducerPath: 'userApi',
    tagTypes: ['user'],
    baseQuery: fetchBaseQuery({ baseUrl: process.env.REACT_APP_SERVER_URL + '/api/user', prepareHeaders: prepare }),
    endpoints: (builder) => ({
        getThisUser: builder.query<User, void>({
            query: () => '/account',
            providesTags: ['user']
        }),
        userLogin: builder.mutation<AuthState, User>({
            query: (body) => ({
                url: '/login',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        }),
        userRegister: builder.mutation<User, User>({
            query: (body) => ({
                url: '/register',
                method: 'POST',
                body
            }),
            invalidatesTags: ['user']
        })
    })
});




export const { useGetThisUserQuery, useUserLoginMutation, useUserRegisterMutation } = userApi
